package ru.konovalov.tm.api.other;

import org.jetbrains.annotations.NotNull;

public interface ISaltSettings {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

}
