package ru.konovalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.model.AbstractOwner;
import ru.konovalov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwner> extends IRepository<E> {

    void clear(@NotNull String userId);

    void clear();

    @Nullable
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @Nullable
    List<E> findAll(@NotNull String userId);

    @Nullable
    E findById(@NotNull String userId, @NotNull String id);

    int size(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project removeById(@NotNull String userId, @NotNull String id);

}
