package ru.konovalov.tm.api.entity;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasCreated {

    @Nullable
    Date getCreated();

    void setCreated(@Nullable Date date);
}
