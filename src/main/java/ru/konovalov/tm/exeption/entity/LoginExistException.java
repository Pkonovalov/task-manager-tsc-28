package ru.konovalov.tm.exeption.entity;

import ru.konovalov.tm.exeption.AbstractException;

public class LoginExistException extends AbstractException {

    public LoginExistException(String value) {
        super("Error. Login '" + value + "' already exist.");
    }

}
