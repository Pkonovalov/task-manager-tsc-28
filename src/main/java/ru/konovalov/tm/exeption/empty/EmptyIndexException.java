package ru.konovalov.tm.exeption.empty;

import ru.konovalov.tm.exeption.AbstractException;

public class EmptyIndexException extends AbstractException {

    public EmptyIndexException() {
        super("Error. Index is empty");
    }

}

