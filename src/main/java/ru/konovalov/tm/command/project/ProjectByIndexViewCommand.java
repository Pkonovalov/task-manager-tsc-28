package ru.konovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.util.TerminalUtil;

public final class ProjectByIndexViewCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-view-by-index";
    }

    @Override
    public @NotNull String description() {
        return "View project by index";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getProjectService().findOneByIndex(userId, TerminalUtil.nextNumber() - 1);
    }

}
