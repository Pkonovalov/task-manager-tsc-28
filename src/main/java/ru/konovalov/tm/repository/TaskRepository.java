package ru.konovalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.repository.ITaskRepository;
import ru.konovalov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Nullable
    @Override
    public List<Task> findALLTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()))
                .collect(Collectors.toList());

    }

    @Nullable
    @Override
    public List<Task> removeAllTaskByProjectId(@Nullable final String projectId, @NotNull final String userId) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()))
                .forEach(this::remove);

        return null;
    }

    @Nullable
    @Override
    public Task assignTaskByProjectId(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        @Nullable final Task task = findOneById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Nullable
    @Override
    public Task unassignTaskByProjectId(@NotNull final String userId, @NotNull final String taskId) {
        @Nullable final Task task = findOneById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public void add(@NotNull final String userId, @NotNull Task task) {
        List<Task> list = findAll(userId);
        tasks.add(task);
    }


    @Override
    public void remove(@NotNull final String userId, @NotNull final Task task) {
        List<Task> list = findAll(userId);
        tasks.remove(task);
    }

    @Override
    public void clear(@NotNull final String userId) {
        List<Task> list = findAll(userId);
        this.tasks.removeAll(list);
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        for (final Task task : tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task removeOneById(@NotNull final String userId, @Nullable final String id) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null);

    }

    @Nullable
    @Override
    public Task removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull final String userId, @Nullable final String name) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null);

    }

    @Nullable
    @Override
    public Task removeOneByName(@NotNull final String userId, @Nullable final String name) {
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Nullable
    @Override
    public void removeAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        for (int i = tasks.size(); i-- > 0; ) {
            if (projectId.equals(tasks.get(i).getProjectId()) && userId.equals(tasks.get(i).getUserId())) {
                tasks.remove(i);
            }
        }
    }

    @NotNull
    @Override
    public int size(@NotNull final String userId) {
        List<Task> list = findAll(userId);
        return tasks.size();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String projectId) {
        for (final Task task : tasks) {
            if (projectId.equals(task.getProjectId()) && userId.equals(task.getUserId())) return true;
        }
        return false;
    }


    @Override
    public boolean existsByName(@NotNull final String userId, @Nullable final String name) {
        for (final Task task : tasks) {
            if (name.equals(task.getName()) && userId.equals(task.getUserId())) return true;
        }
        return false;
    }

    @Override
    public String getIdByIndex(@NotNull int index) {
        return tasks.get(index).getId();
    }

}








